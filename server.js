const express = require('express')
const app = express()
const port = 5000

app.get('/api/localisations/list', (req, res) => {
    const response = {
        "map": [
            {
                'lat': 51.104716,
                'lon': 17.026900,
                'startTimestamp': 1584800000,
                'endTimestamp': 1584800100
            },
            {
                'lat': 51.103426,
                'lon': 17.027622,
                'startTimestamp': 1584800300,
                'endTimestamp': 1584800350
            },
            {
                'lat': 51.070542,
                'lon': 17.020593,
                'startTimestamp': 1584800600,
                'endTimestamp': 1584800900
            }
        ]
    }
    res.json(response);
});

app.listen(port, () => console.log("JOŁ"));

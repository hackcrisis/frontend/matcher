// area has to be / 0
const MINIMUM_AREA = 200
const MINIMUM_SIDE = 30
const _ = require('lodash');

function computeArea(localisationHistory) {

    const lats = _.map(localisationHistory, 'lat');
    const lons = _.map(localisationHistory, "lon");

    let [lower, upper, height] = checkSide(Math.min(...lats), Math.max(...lats));
    let [left, right, width] = checkSide(Math.min(...lons), Math.max(...lons));
    rescaleRatio = Math.sqrt(MINIMUM_AREA / (height * width)) - 1

    if (rescaleRatio > 0) {
        vertical = rescaleSide(lower, upper, rescaleRatio)
        horizontal = rescaleSide(left, right, rescaleRatio)
    } else {
        vertical = [lower, upper]
        horizontal = [left, right]
    }
    results = {
        x: vertical[1],
        y: horizontal[0],
        height: vertical[1] - vertical[0],
        width: horizontal[1] - horizontal[0]
    }
    return results
}

function checkSide(start, end) {
    result = []
    let len = end - start
    if ((len) < MINIMUM_SIDE) {
        start -= (MINIMUM_SIDE - len) / 2
        end += (MINIMUM_SIDE - len) / 2
        len = MINIMUM_SIDE
    }
    return [start, end, len]
}

function rescaleSide(start, end, rescaleRatio) {
    result = []
    let len = end - start
    result.push(start - rescaleRatio * len / 2)
    result.push(end + rescaleRatio * len / 2)
    return result
}

module.exports = { computeArea }

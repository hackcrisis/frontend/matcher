const localisationHistory = require('./jsonfiles/localisationHistory')
const areaPreparation = require('./areaPreparation')
const incidentsFiltering = require('./incidentsFiltering');
const communication = require("./communicationModule");
const express = require('express')
const app = express()
const port = 3000


async function main() {
    dangerousIncidents = await getDangerousIncidents(localisationHistory);
    console.log(dangerousIncidents);
}


async function getDangerousIncidents(localisationHistory) {
    const area = areaPreparation.computeArea(localisationHistory);
    let incidents = null;
    try {
        incidents = await communication.fetchIncidentsInArea(area);
        incidents = incidentsFiltering.filterIncidents(localisationHistory, incidents);
    }
    catch (error) {
        console.error(`[${new Date().toLocaleString()}] Data fetch from server failed!`);
    }
    return incidents;
}


app.get('/', (req, res) => res.send('Hello World!'))
app.listen(port, async () => main());

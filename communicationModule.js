const axios = require("axios");

const URL = "http://127.0.0.1:5000/api/localisations/list";

async function fetchIncidentsInArea(area) {
    const { x, y, height, width } = area;
    const response = await axios.get(URL, {
        params: {
            "x": x,
            "y": y,
            "height": height,
            "width": width
        }
    });
    return response.data.map;
}

module.exports = { fetchIncidentsInArea }
